﻿
var app = angular.module('AngularAuthApp', ['ngRoute', 'LocalStorageModule', 'angular-loading-bar']);

app.config(function ($routeProvider) {

    $routeProvider.when("/home", {
        controller: "homeController",
        templateUrl: "/app/views/home.html"
    });

    $routeProvider.when("/login", {
        controller: "loginController",
        templateUrl: "/app/views/login.html"
    });

    
    $routeProvider.when("/callback/:response", {
        controller: "callbackController",
        templateUrl: "/app/views/message.html"
    });

    $routeProvider.when("/signup", {
        controller: "signupController",
        templateUrl: "/app/views/signup.html"
    });

    $routeProvider.otherwise({ redirectTo: "/home" });

});

var identityServerBaseUri = 'https://localhost:44333/core';
var identityManagerBaseUri = 'http://localhost:37047/';

app.constant('resourceUriProvider', {
    apiIdentityServerBaseUri: identityServerBaseUri,
    apiIdentityManagerBaseUri: identityManagerBaseUri
});

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});

app.run(['idmTokenManagerService', function (idmTokenManagerService) {
    idmTokenManagerService.fillAuthData();
}]);