﻿'use strict';
app.controller('callbackController', ['$scope', '$q', '$location', '$routeParams', 'idmTokenManagerService', function ($scope, $q, $location, $routeParams, idmTokenManagerService) {

    var hash = $routeParams.response;
    if (hash.charAt(0) === "&") {
        hash = hash.substr(1);
    }

    $q.when(idmTokenManagerService.mgr.processTokenCallbackAsync(hash)).then(
        function () {
            idmTokenManagerService.authentication.isAuth = true;
            idmTokenManagerService.authentication.userName = idmTokenManagerService.mgr.profile.preferred_username;
            $location.url("/");
        }, function (error) {
            //idmErrorService.error(error && error.message || error);
        });
}]);