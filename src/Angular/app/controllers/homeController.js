﻿'use strict';
app.controller('homeController', ['$scope', '$location', 'resourceUriProvider', 'idmTokenManagerService', 'linkAuthProviderService', 'claimsService', function ($scope, $location, resourceUriProvider, idmTokenManagerService, linkAuthProviderService, claimsService) {
   
    $scope.hasGoogle = false;
    $scope.hasFacebook = false;

    if (idmTokenManagerService.authentication.isAuth) {
        claimsService.getLinkedAccountClaims().then(function(results) {
            var linkedAccountClaims = results.data.linked_account_claims;
            for (var i = 0; i < linkedAccountClaims.length; i++) {
                if (linkedAccountClaims[i].ProviderName === 'Google') {
                    $scope.hasGoogle = true;
                }
                if (linkedAccountClaims[i].ProviderName === 'Facebook') {
                    $scope.hasFacebook = true;
                }
            }
        }, function(error) {
            alert(error.data.message);
            idmTokenManagerService.mgr.redirectForLogout();
        });
    }

    $scope.logIn = function () {
        idmTokenManagerService.mgr.redirectForToken();
    };

    $scope.unLinkExternalProvider = function (provider) {

        linkAuthProviderService.unlink(provider).then(function (response) {
            $location.url("/");
        },
            function (err) {
                $scope.message = err.message;
            });
    };

    $scope.linkExternalProvider = function (provider) {
        var redirectUri = location.protocol + '//' + location.host + '/linkauthcomplete.html';

        var externalProviderUrl = resourceUriProvider.apiIdentityManagerBaseUri + "api/LinkExternalProvider?provider=" + provider
                                                                    + "&response_type=token&client_id=" + "ngAuthApp2"
                                                                    + "&redirect_uri=" + redirectUri;

        window.$windowScope = $scope;

        var oauthWindow = window.open(externalProviderUrl, "Authenticate Account", "location=0,status=0,width=600,height=750");
    };

    $scope.linkAuthCompletedCB = function (fragment) {

        $scope.$apply(function () {

            var externalData = { provider: fragment.provider, externalAccessToken: fragment.external_access_token };

            linkAuthProviderService.link(externalData).then(function (response) {
                show(response);
                $location.url("/");
            },
                function (err) {
                    show(err);
                });
        });
    };

    $scope.authentication = idmTokenManagerService.authentication;
}]);