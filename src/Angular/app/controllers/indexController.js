﻿'use strict';
app.controller('indexController', ['$scope', '$location', 'idmTokenManagerService', 'linkAuthProviderService', 'claimsService', function ($scope, $location, idmTokenManagerService, linkAuthProviderService, claimsService) {

    $scope.logIn = function () {
        idmTokenManagerService.mgr.redirectForToken();
    };

    $scope.logOut = function () {
        idmTokenManagerService.mgr.redirectForLogout();
    }

    $scope.authentication = idmTokenManagerService.authentication;

    $scope.renewToken = function () {
        idmTokenManagerService.mgr.renewTokenSilentAsync().then(function () {
            show("Silent Renew Successful");
        }, window.showError);
    }

    $scope.getTokenClaims = function () {
        claimsService.getClaims().then(function (results) {
            show(results.data);
        }, function (error) {
            alert(error.data.message);
        });
    }

    $scope.getLinkedAccountClaims = function () {
        claimsService.getLinkedAccountClaims().then(function (results) {
            show(results.data);
        }, function (error) {
            alert(error.data.message);
        });
    }

    $scope.getUserInfo = function () {
        claimsService.getUserInfo().then(function (results) {
            show(results.data);
        }, function (error) {
            alert(error.data.message);
        });
    }
}]);