﻿'use strict';
app.factory('authInterceptorService', ['$q', '$injector','$location', 'localStorageService',  function ($q, $injector,$location, localStorageService) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        var idmTokenManagerService = $injector.get('idmTokenManagerService');

        config.headers = config.headers || {};

        var token = idmTokenManagerService.mgr.access_token;
        if (token) {
            config.headers.Authorization = 'Bearer ' + token;            
        }

        return config;
    }

    var _responseError = function (rejection) {
        var idmTokenManagerService = $injector.get('idmTokenManagerService');
        if (rejection.status === 401) {
            
            $location.url("/");
            //idmTokenManagerService.mgr.redirectForLogout();
        }
        if (rejection.status === 403) {
            $location.url("/");
            //idmTokenManagerService.mgr.redirectForLogout();
        }
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);