﻿'use strict';
app.factory('claimsService', ['$http', 'resourceUriProvider', function ($http, resourceUriProvider) {

    var claimsServiceFactory = {};

    var _getClaims = function () {

        return $http.get(resourceUriProvider.apiIdentityManagerBaseUri + 'api/claims/tokenclaims').then(function (results) {
            return results;
        });
    };

    var _getLinkedAccountClaims = function () {

        return $http.get(resourceUriProvider.apiIdentityManagerBaseUri + 'api/claims/linkedaccountclaims').then(function (results) {
            return results;
        });
    };

    var _getUserInfo = function () {        
        return $http.get(resourceUriProvider.apiIdentityServerBaseUri + "/connect/userinfo").then(function (results) {
            return results;
        });
    };

    claimsServiceFactory.getClaims = _getClaims;
    claimsServiceFactory.getLinkedAccountClaims = _getLinkedAccountClaims;
    claimsServiceFactory.getUserInfo = _getUserInfo;

    return claimsServiceFactory;

}]);