﻿'use strict';
app.factory('idmTokenManagerService', [ 'resourceUriProvider', function (resourceUriProvider) {
    
    var idmTokenManagerServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: ""
    };

    var config = {
        authority: resourceUriProvider.apiIdentityServerBaseUri,
        client_id: "implicitclient",
        redirect_uri: window.location.protocol + "//" + window.location.host + "/#/callback/",

        acr_values: ["redirect_register_uri:" + window.location.protocol + "//" + window.location.host + "/#/register/"],
        post_logout_redirect_uri: window.location.protocol + "//" + window.location.host + "/",
        response_type: "id_token token",
        scope: "openid profile email roles read write api1",
            
        silent_redirect_uri: window.location.protocol + "//" + window.location.host + "/frame.html",
        
        silent_renew: true
    };

    var _mgr = new OidcTokenManager(config);

    var _fillAuthData = function () {

        var token = _mgr.access_token;
        if (token) {
            _authentication.isAuth = true;
            _authentication.userName = _mgr.profile.preferred_username;
        }

    };

    idmTokenManagerServiceFactory.mgr = _mgr;
    idmTokenManagerServiceFactory.authentication = _authentication;
    idmTokenManagerServiceFactory.fillAuthData = _fillAuthData;
    

    return idmTokenManagerServiceFactory;

}]);