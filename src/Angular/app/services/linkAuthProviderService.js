﻿'use strict';
app.factory('linkAuthProviderService', ['$http', '$q', 'localStorageService', 'resourceUriProvider', function ($http, $q, localStorageService, resourceUriProvider) {

    var _externalAuthData = {
        provider: "",
        userName: "",
        externalAccessToken: ""
    };

    var linkAuthProviderServiceFactory = {};

    var _unlink = function(provider) {

        return $http.get(resourceUriProvider.apiIdentityManagerBaseUri + 'api/UnLinkExternalProvider?provider=' + provider).then(function (results) {
            return results;
        });
    };

    var _link = function (registerExternalData) {
        
        var deferred = $q.defer();

        $http.post(resourceUriProvider.apiIdentityManagerBaseUri + 'api/LinkExternalProvider', registerExternalData).success(function (response) {

            deferred.resolve(response);

        }).error(function (response) {
            
              deferred.reject(response);
        });

        return deferred.promise;
    };

    var _getExternalAuthProviders = function() {
        var deferred = $q.defer();

        $http.get(resourceServiceBase + 'api/GetExternalProvider').success(function (response) {

            deferred.resolve(response);

        }).error(function (err) {
            deferred.reject(err);
        });

        return deferred.promise;
    };

    linkAuthProviderServiceFactory.getExternalAuthProviders = _getExternalAuthProviders;
    linkAuthProviderServiceFactory.link = _link;
    linkAuthProviderServiceFactory.externalAuthData = _externalAuthData;
    linkAuthProviderServiceFactory.unlink = _unlink;
    

    return linkAuthProviderServiceFactory;

}]);