﻿/*
 * Copyright 2015 Darren Schwarz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Karama.Identity.Core.MR;

namespace Karama.IdentityManager.Api
{
    [RoutePrefix("api/claims")]
    [Authorize]
    public class ClaimsController : ApiController
    {
        private readonly CustomUserAccountService _customUserAccountService;

        public ClaimsController()
        {
            _customUserAccountService = new CustomUserAccountService(new CustomConfig(), new CustomUserRepository(new CustomDatabase("CustomMembershipReboot")));
        }

        [Route("tokenclaims")]
        [HttpGet]
        public IHttpActionResult TokenClaims()
        {

           // var cp = User as ClaimsPrincipal;
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

            Claim sub = null;

            if (principal != null)
            {
                sub = principal.Claims.SingleOrDefault(c => c.Type == "sub");
            }

            if (sub == null) throw new ArgumentNullException("sub");

            var claims = new Dictionary<string, object>();
            var groups =
                from c in principal.Claims
                group c by c.Type into g
                select g;
            foreach (var item in groups)
            {
                if (item.Count() > 1)
                {
                    claims.Add(item.Key, item.Select(x => x.Value).ToArray());
                }
                else
                {
                    claims.Add(item.Key, item.First().Value);
                }
            }

            return Json(new { message = "access_token_claims called", access_token_claims = claims });
        }

        [Route("linkedaccountclaims")]
        [HttpGet]
        public IHttpActionResult LinkedAccountClaims()
        {

            //var cp = User as ClaimsPrincipal;
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

            Claim sub = null;

            if (principal != null)
            {
                sub = principal.Claims.SingleOrDefault(c => c.Type == "sub");
            }

            if (sub == null) throw new ArgumentNullException("sub");
            var userId = new Guid(sub.Value);
            var customUser = _customUserAccountService.GetByID(userId);
            var linkedAccountClaims = customUser.LinkedAccountClaims;           

            return Json(new { message = "LinkedAccountClaims called", linked_account_claims = linkedAccountClaims });
        }
    }
}