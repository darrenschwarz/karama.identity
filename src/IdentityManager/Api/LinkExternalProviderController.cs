﻿/*
 * Copyright 2015 Darren Schwarz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using Karama.Identity.Core.MR;
using Karama.IdentityManager.Results;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Karama.IdentityManager.Api
{
    [Authorize]
    [RoutePrefix("api/LinkExternalProvider")]
    public class LinkExternalProviderController : ApiController
    {
        private readonly CustomUserAccountService _customUserAccountService;

        public LinkExternalProviderController()
        {
            _customUserAccountService = new CustomUserAccountService(new CustomConfig(),
                new CustomUserRepository(new CustomDatabase("CustomMembershipReboot")));
        }

        // GET api/Account/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> Get(string provider, string error = null)
        {
            string redirectUri = string.Empty;

            if (error != null)
            {
                return BadRequest(Uri.EscapeDataString(error));
            }

            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);



            if (!User.Identity.IsAuthenticated || !externalLogin.LoginProvider.Equals(provider))
            {
                return new ChallengeResult(provider, this);
            }

            var redirectUriValidationResult = ValidateClientAndRedirectUri(Request, ref redirectUri);

            if (!string.IsNullOrWhiteSpace(redirectUriValidationResult))
            {
                return BadRequest(redirectUriValidationResult);
            }

            if (externalLogin.LoginProvider == null)
            {
                return InternalServerError();
            }

            bool hasRegistered = false;

            redirectUri = string.Format("{0}#external_access_token={1}&provider={2}&haslocalaccount={3}&external_user_name={4}",
                                            redirectUri,
                                            externalLogin.ExternalAccessToken,
                                            externalLogin.LoginProvider,
                                            hasRegistered.ToString(),
                                            externalLogin.UserName);

            return Redirect(redirectUri);
        }

        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Post(RegisterExternalBindingModel registerExternalBindingModel)
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

            var sub = principal.Claims.SingleOrDefault(c => c.Type == "sub");

            if (null == sub)
            {
                return BadRequest(HttpStatusCode.Forbidden + " : " + "You can only manage providers logged in with you primary local account");
            }

            var userId = new Guid(sub.Value);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ParsedExternalAccessToken parsedExternalAccessToken =  await VerifyExternalAccessToken(registerExternalBindingModel.Provider, registerExternalBindingModel.ExternalAccessToken);

            try
            {
                CustomUser customUser = _customUserAccountService.GetByID(userId);
                _customUserAccountService.AddOrUpdateLinkedAccount(customUser, registerExternalBindingModel.Provider, parsedExternalAccessToken.user_id, parsedExternalAccessToken.claims);

            }
            catch (ValidationException ex)
            {
                return BadRequest(HttpStatusCode.Forbidden + " : " + ex.Message);
            }
            

            return Ok();

            

        }

        #region Helpers


        private string ValidateClientAndRedirectUri(HttpRequestMessage request, ref string redirectUriOutput)
        {

            Uri redirectUri;

            var redirectUriString = GetQueryString(Request, "redirect_uri");

            if (string.IsNullOrWhiteSpace(redirectUriString))
            {
                return "redirect_uri is required";
            }

            bool validUri = Uri.TryCreate(redirectUriString, UriKind.Absolute, out redirectUri);

            if (!validUri)
            {
                return "redirect_uri is invalid";
            }

            var clientId = GetQueryString(Request, "client_id");

            if (string.IsNullOrWhiteSpace(clientId))
            {
                return "client_Id is required";
            }

            redirectUriOutput = redirectUri.AbsoluteUri;

            return string.Empty;

        }

        private string GetQueryString(HttpRequestMessage request, string key)
        {
            var queryStrings = request.GetQueryNameValuePairs();

            if (queryStrings == null) return null;

            var match = queryStrings.FirstOrDefault(keyValue => string.Compare(keyValue.Key, key, true) == 0);

            if (string.IsNullOrEmpty(match.Value)) return null;

            return match.Value;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }
            public string ExternalAccessToken { get; set; }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer) || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name),
                    ExternalAccessToken = identity.FindFirstValue("ExternalAccessToken"),
                };
            }
        }

        private async Task<ParsedExternalAccessToken> VerifyExternalAccessToken(string provider, string accessToken)
        {
            ParsedExternalAccessToken parsedToken = null;

            var verifyTokenEndPoint = "";

            if (provider == "Facebook")
            {
                //You can get it from here: https://developers.facebook.com/tools/accesstoken/
                //More about debug_tokn here: http://stackoverflow.com/questions/16641083/how-does-one-get-the-app-access-token-for-debug-token-inspection-on-facebook
                var appToken = "1617509121824168|U-Kz0PRxEa1QJ80G0R_aADzLlBk";
                verifyTokenEndPoint = string.Format("https://graph.facebook.com/debug_token?input_token={0}&access_token={1}", accessToken, appToken);
            }
            else if (provider == "Google")
            {
                verifyTokenEndPoint = string.Format("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token={0}", accessToken);
            }
            else
            {
                return null;
            }

            var client = new HttpClient();
            var uri = new Uri(verifyTokenEndPoint);
            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();

                dynamic jObj = (JObject)JsonConvert.DeserializeObject(content);

                parsedToken = new ParsedExternalAccessToken();

                if (provider == "Facebook")
                {
                    parsedToken.user_id = jObj["data"]["user_id"];
                    parsedToken.app_id = jObj["data"]["app_id"];

                    parsedToken.claims = new List<Claim>
                    {
                        new Claim("user_id", jObj["data"]["user_id"].ToString()),
                        new Claim("app_id", jObj["data"]["app_id"].ToString()),
                    };

                    if (!string.Equals(Startup.FacebookAuthOptions.AppId, parsedToken.app_id, StringComparison.OrdinalIgnoreCase))
                    {
                        return null;
                    }
                }

                if (provider == "Google")
                {
                    parsedToken.user_id = jObj["user_id"];
                    parsedToken.app_id = jObj["audience"];
                    parsedToken.claims = new List<Claim>
                    {
                        new Claim("issued_to", jObj["issued_to"].ToString()),
                        new Claim("audience", jObj["audience"].ToString()),
                        new Claim("user_id", jObj["user_id"].ToString()),
                        new Claim("scope", jObj["scope"].ToString()),
                        new Claim("expires_in", jObj["expires_in"].ToString()),
                        new Claim("email", jObj["email"].ToString()),
                        new Claim("verified_email", jObj["verified_email"].ToString()),
                        new Claim("access_type", jObj["access_type"].ToString())
                    };

                    if (!string.Equals(Startup.GoogleAuthOptions.ClientId, parsedToken.app_id, StringComparison.OrdinalIgnoreCase))
                    {
                        return null;
                    }

                }

            }

            return parsedToken;
        }

        public class RegisterExternalBindingModel
        {
            public string UserName { get; set; }

            [Required]
            public string Provider { get; set; }

            [Required]
            public string ExternalAccessToken { get; set; }

        }

        public class ParsedExternalAccessToken
        {
            public string user_id { get; set; }
            public string app_id { get; set; }
            public List<Claim> claims { get; set; }
        }
        #endregion
    }
}