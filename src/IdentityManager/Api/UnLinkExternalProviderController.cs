/*
 * Copyright 2015 Darren Schwarz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using BrockAllen.MembershipReboot;
using Karama.Identity.Core.MR;

namespace Karama.IdentityManager.Api
{
    [Authorize]
    [RoutePrefix("api/UnLinkExternalProvider")]
    public class UnLinkExternalProviderController : ApiController
    {
        private readonly CustomUserAccountService _customUserAccountService;

        public UnLinkExternalProviderController()
        {
            _customUserAccountService = new CustomUserAccountService(new CustomConfig(),
                new CustomUserRepository(new CustomDatabase("CustomMembershipReboot")));
        }

        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Get(string provider)
        {
            ClaimsPrincipal principal = Request.GetRequestContext().Principal as ClaimsPrincipal;

            var sub = principal.Claims.SingleOrDefault(c => c.Type == "sub");

            if (null == sub)
            {
                return BadRequest(HttpStatusCode.Forbidden + " : " + "You can only manage providers logged in with you primary local account");
            }

            var userId = new Guid(sub.Value);

            try
            {
                CustomUser customUser = _customUserAccountService.GetByID(userId);
                var linkedAccountClaim = customUser.LinkedAccountClaims.Select(lac => lac).Where(lac => lac.ProviderName.ToLowerInvariant().Equals(provider.ToLowerInvariant()));

                var linkedAccountClaims = linkedAccountClaim as IList<LinkedAccountClaim> ?? linkedAccountClaim.ToList();
                if (linkedAccountClaims.Any())
                {
                    _customUserAccountService.RemoveLinkedAccountClaims(userId, provider, linkedAccountClaims[0].ProviderAccountID);
                    _customUserAccountService.RemoveLinkedAccount(userId, provider, linkedAccountClaims[0].ProviderAccountID);                    
                }

                return Ok();

            }
            catch (ValidationException ex)
            {
                return BadRequest(HttpStatusCode.Forbidden + " : " + ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(HttpStatusCode.Forbidden + " : " + ex.Message);
            }
        }
    }
}