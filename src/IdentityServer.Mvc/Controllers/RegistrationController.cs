﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Karama.Identity.Core.MR;
using Karama.Identity.Core.MR.UserAccountServiceFacade;
using Karama.IdentityServer.Mvc.Models;

namespace Karama.IdentityServer.Mvc.Controllers
{
    [AllowAnonymous]
    public class RegistrationController : Controller
    {
        private readonly IUserAccountServiceProvider<CustomUser> _customUserAccountService;

        public RegistrationController(IUserAccountServiceProvider<CustomUser> customUserAccountService)
        {
            _customUserAccountService = customUserAccountService;
        }

        [Route("core/registration")]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index(string signin)
        {
            ViewData["SignIn"] = signin;
            return View(new RegistrationModel());
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [Route("core/registration")]
        public ActionResult Index(RegistrationModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var account = CreateAccount(model);
                    if (account == null) throw new ArgumentNullException(nameof(account));

                    ViewData["SignIn"] = model.SignIn;
                    return View("Success", model);
                }
                catch (ValidationException ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return View(model);
        }

        protected CustomUser CreateAccount(RegistrationModel model)
        {
            string tenant = _customUserAccountService.DefaultTenant;
            var account = _customUserAccountService.CreateAccount(tenant, model.Email, model.Password, model.Email);
            return account;
        }
    }
}
