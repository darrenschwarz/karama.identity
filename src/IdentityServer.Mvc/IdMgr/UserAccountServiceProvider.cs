﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Karama.Identity.Core.MR;
using Karama.Identity.Core.MR.UserAccountServiceFacade;

namespace Karama.IdentityServer.Mvc.IdMgr
{
    public class UserAccountServiceProvider : UserAccountServiceProviderBase<CustomUser>
    {
        private readonly CustomUserAccountService _customUserAccountService;

        public UserAccountServiceProvider(CustomUserAccountService customUserAccountService)
        {
            _customUserAccountService = customUserAccountService;
        }

        public override string DefaultTenant
        {
            get
            {
                return _customUserAccountService.Configuration.DefaultTenant;
            }
            set { _customUserAccountService.Configuration.DefaultTenant = value; }
        }

        public override CustomUser CreateAccount(string tenant, string username, string password, string email, Guid? id = null,
            DateTime? dateCreated = null, CustomUser account = null, IEnumerable<Claim> claims = null)
        {
            return _customUserAccountService.CreateAccount(tenant, username, password, email);
        }
    }
}