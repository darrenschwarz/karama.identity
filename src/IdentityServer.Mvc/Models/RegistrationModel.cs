﻿using System.ComponentModel.DataAnnotations;

namespace Karama.IdentityServer.Mvc.Models
{
    public class RegistrationModel
    {
        [ScaffoldColumn(false)]
        public string SignIn { get; set; }

        [Required]
        public string First { get; set; }

        [Required]
        public string Last { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Password confirmation must match password.")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}