﻿using System.Web.Mvc;
using Autofac;
using Autofac.Builder;
using Autofac.Integration.Mvc;
using Karama.Identity.Core.MR;
using Karama.Identity.Core.MR.UserAccountServiceFacade;
using Karama.IdentityServer.Mvc.Controllers;
using Karama.IdentityServer.Mvc.IdMgr;
using Karama.IdentityServer.Mvc.IdSvr;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.Twitter;
using Owin;
using Thinktecture.IdentityServer.Core.Configuration;
using Thinktecture.IdentityServer.Core.Logging;

namespace Karama.IdentityServer.Mvc
{
    
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            LogProvider.SetCurrentLogProvider(new DiagnosticsTraceLogProvider());

            var dataBaseName = "CustomMembershipReboot";

            #region admin app
            //app.Map("/admin", adminApp =>
            //{
            //    var factory = new IdentityManagerServiceFactory();
            //    factory.Configure(dataBaseName);

            //    adminApp.UseIdentityManager(new IdentityManagerOptions()
            //    {
            //        Factory = factory
            //    });
            //});
            #endregion

            #region coreApp
            
            app.Map("/core", coreApp =>
            {
                /*
                    BEGIN: MVC registrations
                    This is so that we can inject dependencies into the additional RegistrationController.
                    _customUserAccountService = new CustomUserAccountService(new CustomConfig(), new CustomUserRepository(new CustomDatabase("CustomMembershipReboot")));
                */

                var builder = new ContainerBuilder();

                builder.RegisterType<UserAccountServiceProvider>()
                    .AsImplementedInterfaces<IUserAccountServiceProvider<CustomUser>, ConcreteReflectionActivatorData>()
                    .InstancePerLifetimeScope();

                builder.RegisterType<CustomUserAccountService>().InstancePerRequest();
                builder.RegisterType<CustomConfig>().InstancePerRequest();
                builder.RegisterType<CustomUserRepository>().InstancePerRequest();
                builder.RegisterType<CustomDatabase>().InstancePerRequest()
                .WithParameter("name", dataBaseName); ;

                builder.RegisterType<RegistrationController>().InstancePerRequest();

                
                var container = builder.Build();
                DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
               
                /*
                    END: MVC registrations
                */


                var idSvrFactory = Factory.Configure();
                idSvrFactory.ConfigureCustomUserService(dataBaseName);

                var options = new IdentityServerOptions
                {
                    IssuerUri = "https://idsrv3.com",
                    SiteName = "Karama IdentityServer Mvc",
                    SigningCertificate = Certificate.Get(),
                    Factory = idSvrFactory,
                    CorsPolicy = CorsPolicy.AllowAll,
                    AuthenticationOptions = new AuthenticationOptions
                    {
                        IdentityProviders = ConfigureAdditionalIdentityProviders,
                        LoginPageLinks = new LoginPageLink[] { 
                            new LoginPageLink{
                                Text = "Register",
                                Href = "../registration"
                            }
                        }                        
                    },

                    EventsOptions = new EventsOptions
                    {
                        RaiseSuccessEvents = true,
                        RaiseErrorEvents = true,
                        RaiseFailureEvents = true,
                        RaiseInformationEvents = true
                    }
                };

                coreApp.UseIdentityServer(options);
            });
            
            #endregion
        }

        public static void ConfigureAdditionalIdentityProviders(IAppBuilder app, string signInAsType)
        {
            var google = new GoogleOAuth2AuthenticationOptions
            {
                AuthenticationType = "Google",
                SignInAsAuthenticationType = signInAsType,
                ClientId = "405547628913-afp6rob22l602dembl7eqnseb9vmrbqs.apps.googleusercontent.com",
                ClientSecret = "ENxb5ZPcOl_BHSWfUTUQecxw",
            };
            app.UseGoogleAuthentication(google);

            var fb = new FacebookAuthenticationOptions
            {
                AuthenticationType = "Facebook",
                SignInAsAuthenticationType = signInAsType,
                AppId = "1617509121824168",
                AppSecret = "dc36301f5ec7a3e30adf3cb6a1a8fddc",
            };
            app.UseFacebookAuthentication(fb);

            var twitter = new TwitterAuthenticationOptions
            {
                AuthenticationType = "Twitter",
                SignInAsAuthenticationType = signInAsType,
                ConsumerKey = "N8r8w7PIepwtZZwtH066kMlmq",
                ConsumerSecret = "df15L2x6kNI50E4PYcHS0ImBQlcGIt6huET8gQN41VFpUCwNjM"
            };
            app.UseTwitterAuthentication(twitter);
        }
    }
}