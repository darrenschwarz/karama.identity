﻿/*
 * Copyright 2015 Darren Schwarz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Karama.Identity.Core.MR;
using Thinktecture.IdentityServer.Core.Configuration;
using Thinktecture.IdentityServer.Core.Models;
using Thinktecture.IdentityServer.Core.Services;
using Thinktecture.IdentityServer.MembershipReboot;

namespace Karama.IdentityServer.IdSvr
{
    public static class CustomUserServiceExtensions
    {
        public static void ConfigureCustomUserService(this IdentityServerServiceFactory factory, string connString)
        {
            factory.UserService = new Registration<IUserService, CustomUserService>();
            factory.Register(new Registration<CustomUserAccountService>());
            factory.Register(new Registration<CustomConfig>(resolver => new CustomConfig()));
            factory.Register(new Registration<CustomUserRepository>());
            factory.Register(new Registration<CustomDatabase>(resolver => new CustomDatabase(connString)));
        }
    }
    
    public class CustomUserService : MembershipRebootUserService<CustomUser>
    {
        public CustomUserService(CustomUserAccountService userSvc)
            : base(userSvc)
        {
            
        }

        public async override Task<AuthenticateResult> AuthenticateExternalAsync(ExternalIdentity externalUser, SignInMessage message)
        {
            if (externalUser == null)
                throw new ArgumentNullException("externalUser");
            AuthenticateResult authenticateResult;
            try
            {                
                string tenant = string.IsNullOrWhiteSpace(message.Tenant) ? userAccountService.Configuration.DefaultTenant : message.Tenant;
                CustomUser acct = userAccountService.GetByLinkedAccount(tenant, externalUser.Provider, externalUser.ProviderId);
                authenticateResult = (object)acct != null ? 
                    await ProcessExistingExternalAccountAsync(acct.ID, externalUser.Provider, externalUser.ProviderId, externalUser.Claims) : 
                    await Task.FromResult<AuthenticateResult>(new AuthenticateResult("/core/admin#/users/create", externalUser.ProviderId, externalUser.ProviderId, identityProvider: externalUser.Provider));
                    //await this.ProcessNewExternalAccountAsync(tenant, externalUser.Provider, externalUser.ProviderId, externalUser.Claims);
                return authenticateResult;
            }
            catch (ValidationException ex)
            {
                authenticateResult = new AuthenticateResult(ex.Message);
            }
            return authenticateResult;

            //return await base.AuthenticateExternalAsync(externalIdentity, signInMessage);
        }

        public static Dictionary<string, string> GetAcrValuesDictionary(IEnumerable<string> acrValues)
        {
            return acrValues.Select(acrValue => acrValue.Split(Convert.ToChar(":"))).ToDictionary(keyValue => keyValue[0], keyValue => keyValue[1]);
        }

    }
}
