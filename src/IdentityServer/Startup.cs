﻿/*
 * Copyright 2015 Darren Schwarz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using Karama.IdentityServer.IdMgr;
using Karama.IdentityServer.IdSvr;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Owin;
using Thinktecture.IdentityManager.Configuration;
using Thinktecture.IdentityServer.Core.Configuration;

namespace Karama.IdentityServer
{
    internal class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var connectionString = "CustomMembershipReboot";

            app.Map("/admin", adminApp =>
            {
                var factory = new IdentityManagerServiceFactory();
                factory.Configure(connectionString);

                adminApp.UseIdentityManager(new IdentityManagerOptions()
                {
                    Factory = factory
                });
            });


            var idSvrFactory = Factory.Configure();
            idSvrFactory.ConfigureCustomUserService(connectionString);

            var options = new IdentityServerOptions
            {
                IssuerUri = "https://idsrv3.com",
                SiteName = "Karama IdentityServer",                
                SigningCertificate = Certificate.Get(),
                Factory = idSvrFactory,
                CorsPolicy = CorsPolicy.AllowAll,
                AuthenticationOptions = new AuthenticationOptions{
                    IdentityProviders = ConfigureAdditionalIdentityProviders,
                }
            };

            app.UseIdentityServer(options);
        }

        public static void ConfigureAdditionalIdentityProviders(IAppBuilder app, string signInAsType)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions());

            var google = new GoogleOAuth2AuthenticationOptions
            {
                AuthenticationType = "Google",
                SignInAsAuthenticationType = signInAsType,
                ClientId = "405547628913-afp6rob22l602dembl7eqnseb9vmrbqs.apps.googleusercontent.com",
                ClientSecret = "ENxb5ZPcOl_BHSWfUTUQecxw",
            };
            app.UseGoogleAuthentication(google);

            var fb = new FacebookAuthenticationOptions
            {
                AuthenticationType = "Facebook",
                SignInAsAuthenticationType = signInAsType,
                AppId = "1617509121824168",
                AppSecret = "dc36301f5ec7a3e30adf3cb6a1a8fddc",
            };
            app.UseFacebookAuthentication(fb);
        }
    }
}