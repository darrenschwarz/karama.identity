﻿/*
 * Copyright 2015 Darren Schwarz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using BrockAllen.MembershipReboot;

namespace Karama.Identity.Core.MR
{
    public class CustomConfig : MembershipRebootConfiguration<CustomUser>
    {
        //public bool EmailIsUsername { get; set; }
        //public static readonly CustomConfig Config;

        public CustomConfig()
        {
            //Config = new CustomConfig();
            PasswordHashingIterationCount = 10000;
            RequireAccountVerification = false;
            EmailIsUsername = true;
        }

        //static CustomConfig()
        //{
        //    Config = new CustomConfig();
        //    Config.PasswordHashingIterationCount = 10000;
        //    Config.RequireAccountVerification = false;
        //    Config.EmailIsUsername = true;                  
        //}
    }
}