﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using BrockAllen.MembershipReboot;

namespace Karama.Identity.Core.MR.UserAccountServiceFacade
{
    public interface IUserAccountServiceProvider<TAccount> where TAccount : UserAccount
    {
        string DefaultTenant { get; set; }

        TAccount CreateAccount(string tenant, string username, string password, string email, Guid? id = null,
            DateTime? dateCreated = null, TAccount account = null, IEnumerable<Claim> claims = null);
    }
}
