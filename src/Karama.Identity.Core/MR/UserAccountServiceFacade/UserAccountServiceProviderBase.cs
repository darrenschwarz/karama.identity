﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using BrockAllen.MembershipReboot;

namespace Karama.Identity.Core.MR.UserAccountServiceFacade
{
    public abstract class UserAccountServiceProviderBase<TAccount> : IUserAccountServiceProvider<TAccount> where TAccount : UserAccount
    {
        public abstract string DefaultTenant { get; set; }

        public abstract TAccount CreateAccount(string tenant, string username, string password, string email, Guid? id = null,
            DateTime? dateCreated = null, TAccount account = default(TAccount), IEnumerable<Claim> claims = null);
    }
}