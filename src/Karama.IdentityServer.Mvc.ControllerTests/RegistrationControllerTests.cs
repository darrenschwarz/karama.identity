﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Karama.Identity.Core.MR;
using Karama.Identity.Core.MR.UserAccountServiceFacade;
using Karama.IdentityServer.Mvc.Controllers;
using Karama.IdentityServer.Mvc.Models;
using Moq;
using NUnit.Framework;
using TestStack.FluentMVCTesting;

namespace Karama.IdentityServer.Mvc.ControllerTests
{
    [TestFixture]
    public class RegistrationControllerTests
    {
        private Mock<IUserAccountServiceProvider<CustomUser>> _customUserAccountServiceMock;

        [SetUp]
        public void SetUp()
        {
            _customUserAccountServiceMock = new Mock<IUserAccountServiceProvider<CustomUser>>();
            _customUserAccountServiceMock.Setup(x => x.DefaultTenant).Returns("default");
            _customUserAccountServiceMock.Setup(
                x => x.CreateAccount(It.IsAny<string>(), "forcesuccess@forcesuccess.com", It.IsAny<string>(), It.IsAny<string>(), null, null, null, null))
                .Returns(new CustomUser());
            _customUserAccountServiceMock.Setup(
                x => x.CreateAccount(It.IsAny<string>(), "forcefail@forcefail.com", It.IsAny<string>(), It.IsAny<string>(), null, null, null, null))
                .Throws(new ValidationException("Validation Error"));
        }

        [Test]
        public void RegistrationController_CalledWithSignIString_ShouldRenderDefaultView()
        {
            var sut = new RegistrationController(_customUserAccountServiceMock.Object);

            sut.WithCallTo(x => x.Index("9e2551b17d891245108b300484e4e9d3")).ShouldRenderDefaultView();
        }

        [Test]
        public void RegistrationController_CalledWithSignInString_SetsViewDataSingIn()
        {
            var sut = new RegistrationController(_customUserAccountServiceMock.Object);

            var result = sut.Index("9e2551b17d891245108b300484e4e9d3") as ViewResult; ;
            if (result == null) throw new ArgumentNullException("");
            {
                ViewDataDictionary viewData = result.ViewData;

                Assert.AreEqual(viewData["SignIn"], "9e2551b17d891245108b300484e4e9d3");
            }
        }

        [Test]
        public void RegistrationController_CreateAccountReturnsUser_ShouldRenderSuccessView()
        {
            var sut = new RegistrationController(_customUserAccountServiceMock.Object);

            var regModel = new RegistrationModel();
            regModel.Email = "forcesuccess@forcesuccess.com";
            sut.WithCallTo(x => x.Index(regModel)).ShouldRenderView("Success");
        }

        [Test]
        public void RegistrationController_CreateAccountThrowsValidationException_ShouldRenderDefaultView()
        {
            var sut = new RegistrationController(_customUserAccountServiceMock.Object);

            var regModel = new RegistrationModel();
            regModel.Email = "forcefail@forcefail.com";

            sut.WithCallTo(x => x.Index(regModel)).ShouldRenderDefaultView();
        }

        [Test]
        public void RegistrationController_CreateAccountReturnsNull_ShouldThrowError()
        {
            var sut = new RegistrationController(_customUserAccountServiceMock.Object);

            var regModel = new RegistrationModel();

            ArgumentNullException ex = Assert.Throws<ArgumentNullException>(() => sut.Index(regModel));

            Assert.AreEqual(ex.ParamName, "account");
        }
    }
}
