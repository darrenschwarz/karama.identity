﻿using System;
using TestStack.Seleno.Configuration;

namespace Karama.IdentityServer.Mvc.FunctionalUITests
{
    public static class BrowserHost
    {
        public static readonly SelenoHost Instance = new SelenoHost();
        public static readonly string RootUrl;

        static BrowserHost()
        {
            Instance.Run("IdentityServer.Mvc", 44333, config => config.WithRouteConfig(RouteConfig.RegisterRoutes));
            RootUrl = Instance.Application.Browser.Url.Replace("http://", "https://");
        }
    }
}