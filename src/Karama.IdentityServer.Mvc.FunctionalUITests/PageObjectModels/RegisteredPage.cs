﻿using TestStack.Seleno.PageObjects;
using TestStack.Seleno.PageObjects.Locators;

namespace Karama.IdentityServer.Mvc.FunctionalUITests.PageObjectModels
{
    public class RegisteredPage : Page
    {
        public string RegisteredMessage
        {
            get
            {
                return Find.Element(By.TagName("h2")).Text;
            }
        }
    }
}