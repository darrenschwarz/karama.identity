﻿using Karama.IdentityServer.Mvc.Models;
using TestStack.Seleno.PageObjects;
using TestStack.Seleno.PageObjects.Locators;

namespace Karama.IdentityServer.Mvc.FunctionalUITests.PageObjectModels
{
    public class RegistrationPage : Page<RegistrationModel>
    {
        public T SubmitRegistration<T>(RegistrationModel registration) where T : UiComponent, new()
        {
            Input.Model(registration);

            return Navigate.To<T>(By.ClassName("btn-primary"));
        }

        public string ValidationErrors
        {
            get
            {
                return Find.Element(By.ClassName("validation-summary-errors")).Text;
            }
        }

        
    }
}
