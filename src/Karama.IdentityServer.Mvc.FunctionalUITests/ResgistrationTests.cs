﻿using Karama.Identity.Core.MR;
using Karama.IdentityServer.Mvc.Controllers;
using Karama.IdentityServer.Mvc.FunctionalUITests.PageObjectModels;
using Karama.IdentityServer.Mvc.Models;
using NUnit.Framework;

namespace Karama.IdentityServer.Mvc.FunctionalUITests
{
    [TestFixture]
    public class ResgistrationTests
    {
        [SetUp]
        public void SetUp()
        {
            var dataBaseName = "CustomMembershipReboot";
            CustomUserAccountService cas = new CustomUserAccountService(new CustomConfig(), new CustomUserRepository(new CustomDatabase(dataBaseName)));

            var fredExists= cas.GetByEmail("fredexists@a.com");
            var fredDoesntExist = cas.GetByEmail("freddoesntexists@a.com");

            if (null == fredExists)
            {                
                cas.CreateAccount("default", "fredexists@a.com", "p", "fredexists@a.com");
            }

            if (null != fredDoesntExist)
            {
                cas.DeleteAccount(fredDoesntExist.ID);
            }
        }

        [Test]
        public void SubmitRegistration_WithEmailAlreadyInUse_ValidationErrorsContainsDuplicateEmailMesage()
        {
            var registerPage =
                BrowserHost.Instance.NavigateToInitialPage<RegistrationController, RegistrationPage>(
                    x => x.Index("9e2551b17d891245108b300484e4e9d3"));

            var registrationDetails = new RegistrationModel
            {
                SignIn = "9e2551b17d891245108b300484e4e9d3",
                First = "Fred",
                Last = "Smith",
                Email = "fredexists@a.com",
                Password = "p",
                ConfirmPassword = "p"
            };

            var registerErrorPage = registerPage.SubmitRegistration<RegistrationPage>(registrationDetails);

            var registerErrorPageText = registerErrorPage.ValidationErrors;

            Assert.That(registerErrorPageText, Is.EqualTo("Email already in use."));
        }


        [Test]
        public void SubmitRegistration_WithEmailNotAlreadyInUse_ValidationErrorsContainsDuplicateEmailMesage()
        {
            var registerPage =
                BrowserHost.Instance.NavigateToInitialPage<RegistrationController, RegistrationPage>(
                    x => x.Index("9e2551b17d891245108b300484e4e9d3"));

            var registrationDetails = new RegistrationModel
            {
                SignIn = "9e2551b17d891245108b300484e4e9d3",
                First = "Fred",
                Last = "Smith",
                Email = "freddoesntexists@a.com",
                Password = "p",
                ConfirmPassword = "p"
            };

            var registeredPage = registerPage.SubmitRegistration<RegisteredPage>(registrationDetails);

            var registeredPageText = registeredPage.RegisteredMessage;

            Assert.That(registeredPageText, Is.EqualTo("Registration Success"));
        }
    }
}
