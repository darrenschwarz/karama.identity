﻿using System;
using NUnit.Framework;

namespace Karama.IdentityServer.Mvc.FunctionalUITests
{
    [SetUpFixture]
    public class TestFixtureLifecycle : IDisposable
    {
        public void Dispose()
        {
            // Cleanup and close browser
            BrowserHost.Instance.Dispose();
        }
    }
}