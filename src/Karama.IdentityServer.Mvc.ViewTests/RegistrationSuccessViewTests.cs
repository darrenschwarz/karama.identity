﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Karama.IdentityServer.Mvc.Models;

using NUnit.Framework;
using RazorGenerator.Testing;

namespace Karama.IdentityServer.Mvc.ViewTests
{
    [TestFixture]
    public class RegistrationSuccessViewTests
    {
        [Test]
        public void RegistrationSuccess_CalledWithViewDataSignIn_RendersLoginLink()
        {
            var sut = new Views.Registration.Success();

            sut.ViewData["SignIn"] = "123456789";

            var model = new RegistrationModel();
            {
               
            };

            HtmlDocument html = sut.RenderAsHtml(model);

            var href = html.GetElementbyId("loginLink").Attributes["href"];

            Assert.AreEqual("/core/login?signin=123456789", href.Value);
        }

    }
}
