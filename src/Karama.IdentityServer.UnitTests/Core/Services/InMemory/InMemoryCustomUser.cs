﻿/*
 * Copyright 2015 Darren Schwarz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using BrockAllen.MembershipReboot.Relational;

namespace Karama.IdentityServer.UnitTests.Core.Services.InMemory
{
    public class InMemoryCustomUser : RelationalUserAccount
    {
        public string IdString { get; set; }

        [Display(Name="First Name")]
        public virtual string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public virtual string LastName { get; set; }
        public virtual int? Age { get; set; }

        public string Subject { get; set; }

        public bool Enabled { get; set; }

        public new string Username { get; set; }

        public string Password { get; set; }

        public string Provider { get; set; }

        public string ProviderId { get; set; }

        public IEnumerable<Claim> Claims { get; set; }

        public InMemoryCustomUser()
        {
            Claims = new List<Claim>();
        }
    }
}