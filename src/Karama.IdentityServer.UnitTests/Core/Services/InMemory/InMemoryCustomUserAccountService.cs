/*
 * Copyright 2015 Darren Schwarz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using System.Linq;
using BrockAllen.MembershipReboot;

namespace Karama.IdentityServer.UnitTests.Core.Services.InMemory
{
    public class InMemoryUserAccountService : UserAccountService<InMemoryCustomUser>
    {
        public InMemoryUserAccountService(IUserAccountRepository<InMemoryCustomUser> userRepository) : base(userRepository)
        {
        }

        public InMemoryUserAccountService(MembershipRebootConfiguration<InMemoryCustomUser> configuration, IUserAccountRepository<InMemoryCustomUser> userRepository) : base(configuration, userRepository)
        {
        }
    }

    public class InMemoryCustomUserAccountService : UserAccountService<InMemoryCustomUser>
    {
        private readonly List<InMemoryCustomUser> _inMemoryCustomUsers;

        public InMemoryCustomUserAccountService(InMemoryCustomConfig config, InMemoryCustomUserRepository repository,
            List<InMemoryCustomUser> inMemoryCustomUsers)
            : base(config, repository)
        {
            _inMemoryCustomUsers = inMemoryCustomUsers;
        }

        public InMemoryCustomUser GetByLinkedAccount(string tenant, string provider, string providerId)
        {
            var query =
                from u in _inMemoryCustomUsers
                where
                    u.Provider == provider &&
                    u.ProviderId == providerId
                select u;
            var user = query.SingleOrDefault();

            return user;
        }

        public InMemoryCustomUser GetByStringId(string idString)
        {
            var query =
                from u in _inMemoryCustomUsers
                where
                    u.IdString == idString 
                select u;
            var user = query.SingleOrDefault();

            return user;
        }
    }
}