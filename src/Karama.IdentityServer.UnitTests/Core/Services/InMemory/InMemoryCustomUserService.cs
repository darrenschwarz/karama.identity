﻿/*
 * Copyright 2015 Darren Schwarz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BrockAllen.MembershipReboot;
using Thinktecture.IdentityServer.Core;
using Thinktecture.IdentityServer.Core.Models;
using Thinktecture.IdentityServer.MembershipReboot;

namespace Karama.IdentityServer.UnitTests.Core.Services.InMemory
{
    public class InMemoryMembershipRebootUserService : MembershipRebootUserService<InMemoryCustomUser>
    {
        public InMemoryMembershipRebootUserService(UserAccountService<InMemoryCustomUser> userAccountService) : base(userAccountService)
        {
        }

        protected async Task<AuthenticateResult> ProcessExistingExternalAccountAsync(string accountID, string provider, string providerId, IEnumerable<Claim> claims)
        {
            InMemoryCustomUser account = ((InMemoryCustomUserAccountService)userAccountService).GetByStringId(accountID);

            var p = IdentityServerPrincipal.Create(account.Subject, GetDisplayNameForAccount(accountID), Constants.AuthenticationMethods.External, provider);
            //var r = new AuthenticateResult(p);

            var result = new AuthenticateResult(accountID, GetDisplayNameForAccount(accountID), account.Claims, provider, "external");//new AuthenticateResult(p);
            return await Task.FromResult(result);
            //return await SignInFromExternalProviderAsync(accountID, provider);
        }

        protected async Task<AuthenticateResult> SignInFromExternalProviderAsync(string accountID, string provider)
        {
            InMemoryCustomUser account = ((InMemoryCustomUserAccountService)userAccountService).GetByStringId(accountID);
            IEnumerable<Claim> claims = await GetClaimsForAuthenticateResult(account);
            return new AuthenticateResult(accountID.ToString(), GetDisplayNameForAccount(accountID), account.Claims, provider, "external");
        }

        protected override Task<IEnumerable<Claim>> GetClaimsForAuthenticateResult(InMemoryCustomUser account)
        {
            return Task.FromResult<IEnumerable<Claim>>((IEnumerable<Claim>)null);
        }

        protected string GetDisplayNameForAccount(string accountId)
        {
            InMemoryCustomUser byId = ((InMemoryCustomUserAccountService)userAccountService).GetByStringId(accountId);
            string str = (string)null;
            if (DisplayNameClaimType != null)
                str = Enumerable.FirstOrDefault<string>(Enumerable.Select<UserClaim, string>(Enumerable.Where<UserClaim>(byId.ClaimCollection, (Func<UserClaim, bool>)(x => x.Type == DisplayNameClaimType)), (Func<UserClaim, string>)(x => x.Value)));
            if (str == null)
                str = Enumerable.FirstOrDefault<string>(Enumerable.Select<UserClaim, string>(Enumerable.Where<UserClaim>(byId.ClaimCollection, (Func<UserClaim, bool>)(x => x.Type == "name")), (Func<UserClaim, string>)(x => x.Value)));
            if (str == null)
                str = Enumerable.FirstOrDefault<string>(Enumerable.Select<UserClaim, string>(Enumerable.Where<UserClaim>(byId.ClaimCollection, (Func<UserClaim, bool>)(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name")), (Func<UserClaim, string>)(x => x.Value)));
            if (str == null)
                str = byId.Username;
            return str;
        }
    }

    public class InMemoryCustomUserService : InMemoryMembershipRebootUserService//MembershipRebootUserService<InMemoryCustomUser>
    {
        private readonly InMemoryCustomUserAccountService _inMemoryCustomUserAccountService;

        public InMemoryCustomUserService(InMemoryCustomUserAccountService userSvc)
            : base(userSvc)
        {
            _inMemoryCustomUserAccountService = userSvc;
        }

        public async override Task<AuthenticateResult> AuthenticateExternalAsync(ExternalIdentity externalUser, SignInMessage message)
        {
            if (externalUser == null)
                throw new ArgumentNullException("externalUser");
            AuthenticateResult authenticateResult;
            try
            {                
                string tenant = string.IsNullOrWhiteSpace(message.Tenant) ? userAccountService.Configuration.DefaultTenant : message.Tenant;

                InMemoryCustomUser acct = _inMemoryCustomUserAccountService.GetByLinkedAccount(tenant, externalUser.Provider, externalUser.ProviderId);                

                authenticateResult = (object)acct != null 
                    ? await ProcessExistingExternalAccountAsync(acct.IdString, externalUser.Provider, externalUser.ProviderId, externalUser.Claims) 
                    : await Task.FromResult<AuthenticateResult>(new AuthenticateResult("This provider is not associated with any exisiting account. If you haven't registered locally please do so, after which you can manage external providers. Alternatively check who is logged in on this machine to the external provider."));
                return authenticateResult;
            }
            catch (ValidationException ex)
            {
                authenticateResult = new AuthenticateResult(ex.Message);
            }
            return authenticateResult;            
        }

    }
}
