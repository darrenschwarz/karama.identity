/*
 * Copyright 2015 Darren Schwarz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Data.Entity;
using BrockAllen.MembershipReboot;
using BrockAllen.MembershipReboot.Relational;

namespace Karama.IdentityServer.UnitTests.Core.Services.InMemory
{
    public class InMemoryMembershipRebootDbContext<TUserAccount, TGroup> : DbContext
        where TUserAccount : RelationalUserAccount
        where TGroup : RelationalGroup
    {
        public string SchemaName { get; private set; }

        public DbSet<TUserAccount> Users { get; set; }

        public DbSet<TGroup> Groups { get; set; }

        public InMemoryMembershipRebootDbContext()
            : this("MembershipReboot", (string)null)
        {
        }

        public InMemoryMembershipRebootDbContext(string nameOrConnectionString)
            : this(nameOrConnectionString, (string)null)
        {
        }

        public InMemoryMembershipRebootDbContext(string nameOrConnectionString, string schemaName)
            : base(nameOrConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}