﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using FluentAssertions;
using Karama.IdentityServer.UnitTests.Endpoints.Setup;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Thinktecture.IdentityServer.Core;
using Thinktecture.IdentityServer.Core.Models;
using Thinktecture.IdentityServer.Core.ViewModels;
using Xunit;

namespace Karama.IdentityServer.UnitTests.Endpoints
{
    [TestClass]
    public class AuthenticationControllerTests : IdSvrHostTestBase
    {
        public ClaimsIdentity SignInIdentity { get; set; }
        public string SignInId { get; set; }

        protected override void Postprocess(IOwinContext ctx)
        {
            if (SignInIdentity != null)
            {
                var props = new AuthenticationProperties();
                props.Dictionary.Add(Constants.Authentication.SigninId, SignInId);
                if (SignInIdentity.AuthenticationType == Constants.ExternalAuthenticationType)
                {
                    props.Dictionary.Add(Constants.Authentication.KatanaAuthenticationType, "Google");
                }
                ctx.Authentication.SignIn(props, SignInIdentity);
                SignInIdentity = null;
            }
        }

        private HttpResponseMessage GetLoginPage(SignInMessage msg = null)
        {
            msg = msg ?? new SignInMessage() { ReturnUrl = Url("authorize") };
            if (msg.ClientId == null) msg.ClientId = TestClients.Get().First().ClientId;

            SignInId = WriteMessageToCookie(msg);

            var resp = Get(Constants.RoutePaths.Login + "?signin=" + SignInId);
            ProcessXsrf(resp);
            return resp;
        }

        [Fact]
        public void LoginExternalCallback_WithNoLinkedAccount_ReturnsErrorMessage()
        {
            var msg = new SignInMessage();
            msg.IdP = "Google";
            msg.ReturnUrl = Url("authorize");
            var resp1 = GetLoginPage(msg);

            var sub = new Claim(Constants.ClaimTypes.Subject, "123456", ClaimValueTypes.String, "Google");
            SignInIdentity = new ClaimsIdentity(new Claim[] { sub }, Constants.ExternalAuthenticationType);
            var resp2 = client.GetAsync(resp1.Headers.Location.AbsoluteUri).Result;
            client.SetCookies(resp2.GetCookies());

            var resp3 = Get(Constants.RoutePaths.LoginExternalCallback);
            var model = resp3.GetModel<ErrorViewModel>();

            resp3.StatusCode.Should().Be(HttpStatusCode.OK);
            model.ErrorMessage.Should().Be("This provider is not associated with any exisiting account. If you haven't registered locally please do so, after which you can manage external providers. Alternatively check who is logged in on this machine to the external provider.");
        }

        [Fact]
        public void LoginExternalCallback_WithLinkedAccount_RedirectsToAuthorizeEndpoint()
        {
            var msg = new SignInMessage(); 

            msg.IdP = "Google";
            msg.ReturnUrl = Url("authorize");
            var resp1 = GetLoginPage(msg);

            var sub = new Claim(Constants.ClaimTypes.Subject, "123", ClaimValueTypes.String, "Google");
            SignInIdentity = new ClaimsIdentity(new Claim[] { sub }, Constants.ExternalAuthenticationType);
            var resp2 = client.GetAsync(resp1.Headers.Location.AbsoluteUri).Result;
            client.SetCookies(resp2.GetCookies());

            var resp3 = Get(Constants.RoutePaths.LoginExternalCallback);
            resp3.StatusCode.Should().Be(HttpStatusCode.Found);
            resp3.Headers.Location.AbsoluteUri.Should().Be(Url("authorize"));
        }
    }
}
